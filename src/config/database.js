module.exports = {
  dialect: "postgres",
  host: "localhost",
  username: "postgres",
  password: "123v",
  database: "sqlnode",
  define: {
    timestamps: true,
    underscored: true,
  },
};
